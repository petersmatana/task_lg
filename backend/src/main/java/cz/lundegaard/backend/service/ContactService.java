package cz.lundegaard.backend.service;

import cz.lundegaard.backend.dto.ContactDto;
import cz.lundegaard.backend.entity.ContactEntity;
import cz.lundegaard.backend.repository.ContactRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

@Component
public class ContactService {
  @Autowired
  @Qualifier("contactRepository")
  private ContactRepository contactRepository;

  @Autowired
  @Qualifier("modelMapper")
  private ModelMapper modelMapper;

  public void saveContact(ContactDto contactDto) {
    ContactEntity contactEntity = modelMapper.map(contactDto, ContactEntity.class);
    this.contactRepository.save(contactEntity);
  }
}
