package cz.lundegaard.backend.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

public class ContactDto {

  @NotNull
  @Pattern(regexp = "^[A-Za-z0-9]+$")
  private String policyNumber;

  @NotNull
  @Pattern(regexp = "^[A-Za-z]+$")
  private String name;

  @NotNull
  @Pattern(regexp = "^[A-Za-z]+$")
  private String surname;

  @NotNull
  @Size(max = 5000)
  private String request;

  public ContactDto() {
  }

  public ContactDto(String policyNumber, String name, String surname, String request) {
    this.policyNumber = policyNumber;
    this.name = name;
    this.surname = surname;
    this.request = request;
  }

  public String getPolicyNumber() {
    return policyNumber;
  }

  public void setPolicyNumber(String policyNumber) {
    this.policyNumber = policyNumber;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String surname) {
    this.surname = surname;
  }

  public String getRequest() {
    return request;
  }

  public void setRequest(String request) {
    this.request = request;
  }

  @Override
  public String toString() {
    return "ContactDto{" +
        "policyNumber='" + policyNumber + '\'' +
        ", name='" + name + '\'' +
        ", surname='" + surname + '\'' +
        ", request='" + request + '\'' +
        '}';
  }
}
