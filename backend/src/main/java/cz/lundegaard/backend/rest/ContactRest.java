package cz.lundegaard.backend.rest;

import cz.lundegaard.backend.dto.ContactDto;
import cz.lundegaard.backend.service.ContactService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/contact")
public class ContactRest {

  @Autowired
  private ContactService contactService;

  @PostMapping
  public ResponseEntity<String> createContact(@Valid @RequestBody final ContactDto contact, BindingResult result) {
    if (result.hasErrors()) {
      System.out.println("error");
      return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    } else {
      System.out.println(contact);
      this.contactService.saveContact(contact);
      return new ResponseEntity<>(HttpStatus.OK);
    }
  }

}
