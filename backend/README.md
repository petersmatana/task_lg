# intro
v prvnim commitu jsem vytvoril backend cast, ktera mi zabrala asi 0.5MD a je na 90% hotova.

# co neni hotove
neni dekompozice "Kind of Request". neni vhodne resit tak ze ulozim Id nebo String daneho typu Requestu. proto bude implementovano do samostatne tabulky

# co je hotove
- post endpoint, ktery vytvori Contact do in-memory databaze h2.
- architektura aplikace: rest controller -> DTO -> service (business) layer -> Mapping DTO na JPARepository

# co by bylo vhodne vyresit lepe
- mam tuseni ze ModelMapper nekdy nefunguje a clovek si musi psat sve: DTO2Entity
- nejsem spokojeny s navratovou hodnotou v RestController. idealni je mit univerzalni logiku, kde vracim data, metadata a technicke veci typu JWT token nebo HTTP status code.
- obecne logovat co se v aplikaci deje, pokud nastane nejaka Exception nebo jina vyjimecna udalsot
- rad bych napsal testy 
